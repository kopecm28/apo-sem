# typedef struct knobs_t
This structure contains values from every one of three knobs

    * uint8_t R
    * uint8_t G
    * uint8_t B
# typedef struct point_t
This structure contains vertical and horizontal coordinates of draw pointer

    * uint16_t x
    * uint16_t y

# int main()
    * main of the code, loops it to draw on lcd
# void loadKnobs(volatile knobs_t *knobs)
    * gets values from each knob
# void setPoint(point_t *newPoint, uint16_t x, uint16_t y)
    * prepares new point for draw
# void show_color_on_led(uint64_t color)
    * set LED´s colors to input value
# void drawPoint(point_t *oldPoint, point_t *newPoint, volatile uint16_t *lcd, uint16_t color_16b)
    * draw new point into the lcd display
# void set_draw_color(uint16_t *color16b, uint64_t *color64b, int wait_time)
    * gets color from input and changes the draw color and led color to it
# void setDrawColor_knob(uint16_t *color16b, uint64_t *color64b, volatile knobs_t *knobs)
    * changes the color based on blue knob value
