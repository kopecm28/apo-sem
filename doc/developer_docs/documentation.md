This is a documentation for GRAFO project.

## Content

Functions and files

* [main.c](main.md)
* [my_terminal.c](my_terminal.md)
* [my_lcd_text.c](my_lcd_text.md)

Block diagram

At startup, the program loads peripherals status, sets drawing color, and writes directly to LCD.
Program loads positions of red and green knobs and in case of a difference it computes a suitable neighbour to the current cursor position. Program can also change the drawing color if the value of blue knob has changed or user used the terminal to change the color.
Current drawing color is indicated by two LEDs.
