**LCD_FB_START 0xffe00000** - start address of lcd display

**LCD_FB_END 0xffe4afff** - end address of lcd display

**GAB 2** - gab between two characters, size two

**LCD_WIDTH 480** - width of lcd display

**LCD_HEIGHT 320** - height of lcd display

**DRAW_BORDER 255** - border in pixels for drawing

uint16_t text_field[LCD_WIDTH * LCD_HEIGHT] - lcd buffer

# structure Gcolor16b

This structure contains colors in 16 bites.

uint16_t red;

uint16_t blue;

uint16_t green;

uint16_t black;

uint16_t white;

uint16_t yellow;

# structure Gcolor64b

This structure contains colors in 64 bites.

uint64_t red;

uint64_t blue;

uint64_t green;

uint64_t black;

uint64_t white;

uint64_t yellow;

# int get_width_of_char(font_descriptor_t *file_des, int ch_char)
    * Function for getting the width of a char from a fond for lcd display
# void color_pixels(uint16_t pix_color, int start_pos, int block_size)
    * Colors pixels in field according to inputs
# void make_draw_frame(uint16_t pix_color)
    * Makes draw frame in field
# int get_text_lenght(font_descriptor_t *file_des, int *text_size, char *str_text)
    * Returns text length according to file descriptor
# void write_character(int ch_size, int start, uint16_t char_color, font_descriptor_t *file_des, char character, int line_number)
    * Writes one character into the text field
# void draw_line(int size_of_font, int line_number, uint16_t text_color, font_descriptor_t *file_des, int idx, char *str_text)
    * Writes one line of a string according to inputs
# void init_lcd_message(int size_of_font, uint16_t text_color, font_descriptor_t *file_des)
    * Draws initialization message to lcd display
# void pick_color(int size_of_font, uint16_t text_color, font_descriptor_t *file_des)
    * Draw color options to lcd
# void black_the_lcd(void)
    * Erases the whole lcd display
# void black_the_draw_frame(void)
    * Erases just the draw frame for new drawing
