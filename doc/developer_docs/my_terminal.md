# volatile int terminalIn()
    * Gets one character from terminal input
# volatile int my_terminalIn(int wait_time)
    * Gets one character from terminal input but with a timeout
# void terminalOut(volatile int print)
    * Writes data to terminal output
# void print_str_on_terminal(char *str)
    * Write string to terminal output
# unsigned long myStrLen(const char *str)
    * Returns length of a string
# void print_coordinates(uint16_t x, uint16_t y)
    * Prints out vertical and horizontal coordinate from input
# char *my_int_to_str_convert(int number, char *outStr)
    * Converts number into string
# void print_num_terminal(int num)
    * Prints number into terminal
# volatile int pick_color_trm(uint16_t *color16b, uint64_t *color64b, int wait_time)
    * Gets color choice from terminal input with and without timeout
