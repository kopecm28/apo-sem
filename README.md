## GRAFO

Seminary work for the APO subject in summer semester of 2020. The GRAFO game for the QTMips sim.

GitLab repository: https://gitlab.fel.cvut.cz/kopecm28/apo-sem

User manual (installation guide etc.): doc/user_manual/user_manual.pdf

Documentation: doc/developer_doc/docs.md

App architecture diagram: doc/developerDoc/block_schema.pdf
